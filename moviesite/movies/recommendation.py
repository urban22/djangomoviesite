from django.db.models import Q
from django.db import connection

from .models import Rating, User


def recommendation(user_id):
    # получение рейтингов пользователя и списка всех пользователей кроме текущего
    user_ratings = Rating.objects.filter(user_id=user_id)
    users = User.objects.filter(~Q(id=user_id))
    '''
        Если у пользователя меньше 3 оценок, то недостаточно
        данных для построения рекомендаций
    '''
    if len(user_ratings) < 3:
        return False
    recommendation_list = []
    for user in users:
        # оценки другого пользователя (хотя бы 3)
        other_user_ratings = Rating.objects.filter(user_id=user.id)
        if len(other_user_ratings) < 3:
            continue
        current_user_general_ratings = []
        other_user_general_ratings = []
        for other_rating in other_user_ratings:
            for user_rating in user_ratings:
                if user_rating.movie_id == other_rating.movie_id:
                    current_user_general_ratings.append(user_rating.value.value)
                    other_user_general_ratings.append(other_rating.value.value)
        if len(current_user_general_ratings) < 2:
            continue
        diff_ratings = [abs(x-y) for x, y in zip(current_user_general_ratings, other_user_general_ratings)]
        diff = sum(diff_ratings)/len(diff_ratings)
        if diff > 1:
            continue
        other_films = get_new_films(user_id, user.id)
        for film in other_films:
            if film[0] not in recommendation_list:
                recommendation_list.append(film[0])
    return recommendation_list


def get_new_films(main_user_id, second_user_id):
    cursor = connection.cursor()
    cursor.execute("""select distinct rat.movie_id
    from movies_rating rat
    inner join movies_ratingvalue rv on rv.id = rat.value_id
    where rat.user_id = %s
    and rv.value >= 4
    and rat.movie_id not in (
        select distinct rat1.movie_id
        from movies_rating rat1
        where rat1.user_id = %s
    )""", [second_user_id, main_user_id])
    row = cursor.fetchall()
    return row


def get_user_ratings(user_id):
    cursor = connection.cursor()
    cursor.execute("""select distinct rat.movie_id as id
    from movies_rating rat
    where rat.user_id = %s
    """, [user_id])
    rows = cursor.fetchall()
    rating_list = []
    for row in rows:
        rating_list.append(row[0])
    return rating_list
