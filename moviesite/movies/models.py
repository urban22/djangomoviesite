from django.contrib.auth.models import User
from django.db import models
from datetime import datetime
from django.db import connection

from django.urls import reverse


class Category(models.Model):
    """"
    Таблица категорий Category содержит информацию о категории
    произведения - фильм, мультфильм, сериал...
    """
    name = models.CharField("Категория", max_length=50)
    description = models.TextField("Описание")
    url = models.SlugField(max_length=50, unique=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Категория"
        verbose_name_plural = "Категории"


class Genre(models.Model):
    """"
    Таблица жанров Genre содержит информацию о жанре
    фильма - мелодрама, детектив, ужасы...
    """
    name = models.CharField("Название", max_length=100)
    description = models.TextField("Описание")
    url = models.SlugField(max_length=100, unique=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Жанр"
        verbose_name_plural = "Жанры"


class Person(models.Model):
    """
    Таблица с информацией о людях Person
    Содержит данные об актерах, режисерах и прочих людях, принимающих
    участие в съемочном процессе
    """
    name = models.CharField("Имя", max_length=100)
    birthdate = models.DateField("Дата рождения", default=datetime.now().day)
    biography = models.TextField("Краткая биография")
    image = models.ImageField("Изображение", upload_to="actors/")

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("person_detail", kwargs={"slug": self.name})

    class Meta:
        verbose_name = "Участник съемочного процесса"
        verbose_name_plural = "Участники съемочного процесса"


class Movie(models.Model):
    """
    Таблица с информацией о фильмах Movie
    Содержит данные о фильмах - название, категория, жанр, актеры, режисеры...
    """
    name = models.CharField("Название", max_length=100)
    year = models.PositiveSmallIntegerField("Год", default=datetime.now().year)
    country = models.CharField("Страна", max_length=50)
    poster = models.ImageField("Постер", upload_to="movies/")
    tagline = models.CharField("Слоган", max_length=100, default='')
    synopsis = models.TextField("Синопсис")
    category = models.ForeignKey(Category, verbose_name="Категория", on_delete=models.SET_NULL, null=True)
    directors = models.ManyToManyField(Person, verbose_name="режиссер", related_name="film_director")
    actors = models.ManyToManyField(Person, verbose_name="актеры", related_name="film_actor")
    genres = models.ManyToManyField(Genre, verbose_name="жанры")
    world_premiere = models.DateField("Примьера в мире", default=datetime.now().day)
    budget = models.PositiveIntegerField("Бюджет", default=0)
    fees_in_usa = models.PositiveIntegerField("Сборы в США", default=0)
    fess_in_world = models.PositiveIntegerField("Сборы в мире", default=0)
    url = models.SlugField(max_length=130, unique=True)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("movie_detail", kwargs={"slug": self.url})

    class Meta:
        verbose_name = "Фильм"
        verbose_name_plural = "Фильмы"


class MovieShots(models.Model):
    """Кадры из фильма"""
    title = models.CharField("Заголовок", max_length=100)
    description = models.TextField("Описание")
    image = models.ImageField("Изображение", upload_to="movie_shots/")
    movie = models.ForeignKey(Movie, verbose_name="Фильм", on_delete=models.CASCADE)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Кадр из фильма"
        verbose_name_plural = "Кадры из фильма"


class RatingValue(models.Model):
    """Значение рейтинга"""
    value = models.SmallIntegerField("Значение рейтинга", default=0)

    def __str__(self):
        return f"{self.value}"

    class Meta:
        verbose_name = "Значение рейтинга"
        verbose_name_plural = "Значение рейтинга"
        ordering = ["-value"]


class Rating(models.Model):
    """Рейтинг"""
    user = models.ForeignKey(User, verbose_name="Пользователь", on_delete=models.CASCADE)
    movie = models.ForeignKey(Movie, verbose_name="Фильм", on_delete=models.CASCADE)
    value = models.ForeignKey(RatingValue, verbose_name="Значение", on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.user} - {self.movie} - {self.value}"

    def averageRating(movie_id):
        cursor = connection.cursor()
        cursor.execute("""with rating as (
  select case when sum(distinct rv.value) is not null then sum(rv.value) else 0 end as rating,
    case when sum(distinct rv.value) is not null then count(rv.value) else 0 end as rating_count
  from movies_movie movie
  inner join movies_rating mr on movie.id = mr.movie_id
  inner join movies_ratingvalue rv on rv.id = mr.value_id
    where movie.id = %s
),
reviews_rating as (
  select case when sum(distinct rv.value) is not null then sum(rv.value) else 0 end as rating,
    case when sum(distinct rv.value) is not null then count(rv.value) else 0 end as rating_count
  from movies_movie movie
  inner join movies_reviews mr on movie.id = mr.movie_id
  inner join movies_reviewtypes rt on rt.id = mr.type_id
  inner join movies_ratingvalue rv on rv.id = rt.rating_id
    where movie.id = %s
)
select
  round(((cast(rating.rating + reviews_rating.rating as float))/(cast(rating.rating_count + reviews_rating.rating_count as float))),2) as awarage_rating
from reviews_rating
cross join rating""", [movie_id, movie_id])
        row = cursor.fetchone()[0]
        return row

    class Meta:
        verbose_name = "Рейтинг"
        verbose_name_plural = "Рейтинг"


class ReviewTypes(models.Model):
    """Типы отзыва к фильму"""
    name = models.CharField("Тип", max_length=50)
    code = models.CharField("Код", max_length=50)
    color = models.CharField("Код", max_length=20)
    rating = models.ForeignKey(RatingValue, verbose_name="Рейтинг", on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Типы отзыва к фильму"
        verbose_name_plural = "Типы отзыва к фильму"


class Reviews(models.Model):
    """Отзывы к фильму"""
    type = models.ForeignKey(ReviewTypes, verbose_name="Тип", on_delete=models.CASCADE)
    movie = models.ForeignKey(Movie, verbose_name="Фильм", on_delete=models.CASCADE)
    user = models.ForeignKey(User, verbose_name="Пользователь", on_delete=models.CASCADE)
    text = models.TextField("Комментарий")

    def __str__(self):
        return f"{self.movie}"

    class Meta:
        verbose_name = "Отзывы к фильму"
        verbose_name_plural = "Отзывы к фильму"


class MovieRatingView(models.Model):
    movie = models.ForeignKey(Movie, verbose_name="Фильм", on_delete=models.CASCADE)
    awarage_rating = models.IntegerField("Значение рейтинга")

    class Meta:
        managed = False
        db_table = "movie_rating_view"
