# Generated by Django 3.1.1 on 2021-03-09 21:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('movies', '0005_auto_20210310_0003'),
    ]

    operations = [
        migrations.AddField(
            model_name='reviewtypes',
            name='color',
            field=models.CharField(default=1, max_length=20, verbose_name='Код'),
            preserve_default=False,
        ),
    ]
