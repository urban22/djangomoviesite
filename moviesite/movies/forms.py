from django import forms

from .models import Reviews, Rating, RatingValue


class ReviewForm(forms.ModelForm):
    """Форма добавления отзыва"""
    class Meta:
        model = Reviews
        fields = ("text", "type")


class RatingForm(forms.ModelForm):
    """Форма добавления рейтинга"""
    value = forms.ModelChoiceField(
        queryset=RatingValue.objects.all(), widget=forms.RadioSelect(), empty_label=None
    )

    class Meta:
        model = Rating
        fields = ("value",)
