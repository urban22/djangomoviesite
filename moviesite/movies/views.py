from os.path import exists

from django.db.models import Q
from django.http import HttpResponse, JsonResponse
from django.shortcuts import redirect
from django.views.generic import ListView, DetailView
from django.views.generic.base import View
from itertools import chain

from .models import Movie, ReviewTypes, Rating, RatingValue, MovieRatingView, Person, Genre
from .forms import ReviewForm, RatingForm
from .recommendation import recommendation, get_user_ratings


class GenreYear:
    def get_genres(self):
        return Genre.objects.all()

    def get_years(self):
        return Movie.objects.all().values("year").order_by("year").distinct()


class MovieView(GenreYear, ListView):
    model = MovieRatingView
    template_name = "movies/movie_list.html"
    paginate_by = 5

    def get_queryset(self):
        current_user = self.request.user
        if current_user.id:
            recommendations = recommendation(current_user.id)
            if recommendations is False:
                queryset = MovieRatingView.objects.order_by('-awarage_rating')
            else:
                user_ratings = get_user_ratings(current_user.id)
                queryset1 = MovieRatingView.objects\
                    .filter(Q(id__in=recommendations))\
                    .order_by('-awarage_rating')
                queryset2 = MovieRatingView.objects\
                    .filter(~Q(id__in=recommendations), ~Q(id__in=user_ratings))\
                    .order_by('-awarage_rating')
                queryset3 = MovieRatingView.objects\
                    .filter(~Q(id__in=recommendations), Q(id__in=user_ratings))\
                    .order_by('-awarage_rating')
                queryset = list(chain(queryset1, queryset2, queryset3))
        else:
            queryset = MovieRatingView.objects.order_by('-awarage_rating')
        return queryset

    def get_context_data(self, **kwargs):
        context = super(ListView, self).get_context_data(**kwargs)
        return context


class MovieDetailView(GenreYear, DetailView):
    model = Movie
    slug_field = "url"
    template_name = "movies/movie_detail.html"

    def get_context_data(self, **kwargs):
        context = super(DetailView, self).get_context_data(**kwargs)
        slug = self.kwargs['slug']
        movie = Movie.objects.get(url=slug).id
        current_user = self.request.user
        current_rating = 6
        b = Rating.objects.filter(user_id=current_user.id, movie_id=movie).exists()
        if b:
            current_rating = int(Rating.objects.get(user_id=current_user.id, movie_id=movie).value.value)

        aw_rating = Rating.averageRating(movie)
        context['types'] = ReviewTypes.objects.all()
        context['aw_rating'] = aw_rating
        context['stars'] = RatingValue.objects.all()
        context['current_rating'] = current_rating
        return context


class AddReview(View):
    def post(self, request, pk):
        form = ReviewForm(request.POST)
        movie = Movie.objects.get(id=pk)
        if form.is_valid():
            form = form.save(commit=False)
            form.movie = movie
            form.user = request.user
            form.save()
        return redirect(movie.get_absolute_url())


class AddRating(View):
    def post(self, request):
        form = RatingForm(request.POST)
        if form.is_valid():
            Rating.objects.update_or_create(
                movie_id=int(request.POST.get("movie")),
                user_id=int(request.POST.get("user")),
                defaults={'value_id': int(request.POST.get("value"))}
            )
            return HttpResponse(status=201)
        else:
            return HttpResponse(status=400)


class FilterMoviesView(GenreYear, ListView):
    template_name = "movies/movie_list.html"

    """Фильтр фильмов в json"""
    def get_queryset(self):
        print(self.request.GET.getlist("year"))
        if len(self.request.GET.getlist("year")) > 0 and len(self.request.GET.getlist("genre")) == 0:
            queryset = MovieRatingView.objects.filter(
                Q(movie__year__in=self.request.GET.getlist("year"))
            ).distinct()
        elif len(self.request.GET.getlist("genre")) > 0 and len(self.request.GET.getlist("year")) == 0:
            queryset = MovieRatingView.objects.filter(
                Q(movie__genres__in=self.request.GET.getlist("genre"))
            ).distinct()
        else:
            queryset = MovieRatingView.objects.filter(
                Q(movie__year__in=self.request.GET.getlist("year")),
                Q(movie__genres__in=self.request.GET.getlist("genre"))
            ).distinct()
        return queryset


class PersonView(GenreYear, DetailView):
    model = Person
    template_name = 'movies/person.html'
    slug_field = 'name'


class Search(ListView):
    template_name = "movies/movie_list.html"

    def get_queryset(self):
        return MovieRatingView.objects.filter(movie__name__icontains=self.request.GET.get("q"))

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context["q"] = f'q={self.request.GET.get("q")}&'
        return context
