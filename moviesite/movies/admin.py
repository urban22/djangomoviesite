from django.contrib import admin

from .models import Category, Genre, Movie, MovieShots, Person, ReviewTypes, Reviews, Rating, RatingValue


admin.site.register(Category)
admin.site.register(Genre)
admin.site.register(Movie)
admin.site.register(MovieShots)
admin.site.register(Person)
admin.site.register(ReviewTypes)
admin.site.register(Reviews)
admin.site.register(Rating)
admin.site.register(RatingValue)
